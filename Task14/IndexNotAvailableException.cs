﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Task14
{
    class IndexNotAvailableException : Exception
    {
        public IndexNotAvailableException()
        {
              Console.WriteLine("Exception: Index not available");
        }

        public IndexNotAvailableException(string message) : base(message)
        {
        }

        public IndexNotAvailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IndexNotAvailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
