﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Task14
{
    public class ItemAtIndexDoesNotExistException : Exception
    {
        public ItemAtIndexDoesNotExistException()
        {
            Console.WriteLine("Exception: Item does not exist at index");
        }

        public ItemAtIndexDoesNotExistException(string message) : base(message)
        {
        }

        public ItemAtIndexDoesNotExistException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ItemAtIndexDoesNotExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
