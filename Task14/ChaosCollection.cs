﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task14
{
    class ChaosCollection<T>
    {
        private int size;
       private List<T> collection;

        public ChaosCollection(int size)
        {
            this.size = size;
            this.collection = CreateList<T>(size);
            Console.WriteLine("Initial collection length: " + this.collection.Count);
        }

        public List<T> Collection { get => collection; set => collection = value; }


        private List<T> CreateList<T>(int capacity)
        {
            return Enumerable.Repeat(default(T), capacity).ToList();
        }

        public void AddRandom(T item)
        {
            Random random = new Random();
            int randomIndex = random.Next(this.size);
            try
            {
                if (this.collection[randomIndex] == null)
                {
                    this.collection[randomIndex] = item;
                }
                else
                {
                    throw new IndexNotAvailableException();
                }
              
            }catch(IndexNotAvailableException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public T GetRandom()
        {
            Random random = new Random();
            int randomIndex = random.Next(this.size);
            T returnedItem;
            try
            {
                returnedItem = this.collection[randomIndex];
                if (returnedItem == null)
                {
                    throw new ItemAtIndexDoesNotExistException();
                }
                else {
                    return returnedItem;
                }
            }
            catch (ItemAtIndexDoesNotExistException ex)
            {
                Console.WriteLine(ex.Message);
                return default(T);
            }
        }
    }
}
