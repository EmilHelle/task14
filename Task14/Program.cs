﻿using System;

namespace Task14
{
    class Program
    {
        static void Main(string[] args)
        {
            //create chaos collection
            ChaosCollection<string> chaosCollectionOfNames = new ChaosCollection<string>(15);
            //try to add random item, return custom exception if not possible
            chaosCollectionOfNames.AddRandom("Josh");
            chaosCollectionOfNames.AddRandom("Julia");
            chaosCollectionOfNames.AddRandom("Simon");
            chaosCollectionOfNames.AddRandom("Gustav");
            chaosCollectionOfNames.AddRandom("Siegfried");
            chaosCollectionOfNames.AddRandom("Brolin");
            chaosCollectionOfNames.AddRandom("Hitler");
            chaosCollectionOfNames.AddRandom("Stalin");
            chaosCollectionOfNames.AddRandom("Gandhi");
            chaosCollectionOfNames.AddRandom("Hulk");

            //show random placement of items in collection
            foreach (string name in chaosCollectionOfNames.Collection)
            {
                if (name == null)
                {
                    Console.WriteLine("_");
                }
                else
                {
                    Console.WriteLine(name);
                }
            }
            //show that size of collection has stayed the same
            Console.WriteLine("chaosCollection length: " + chaosCollectionOfNames.Collection.Count);

            //try to return random item out of collection, return custom exception if not possible
            string item = chaosCollectionOfNames.GetRandom();
            Console.WriteLine("Returned item: " + item);
;
        }
    }
}
